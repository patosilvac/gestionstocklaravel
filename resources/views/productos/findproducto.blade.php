@extends('base')
@section('seccion')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>LISTADO GENERAL DE PRODUCTOS</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <!-- row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card"> 
                            <div class="card-header">
                                <div class="input-group-append">
                                    <div class="col-sm-4">
                                        <select class="form-control" id="inputSucursal">
                                            <option>Seleccionar opcion de busqueda</option>
                                            <option>Codigo</option>
                                            <option>Nombre</option>
                                            <option>Sucursal</option>
                                        </select>
                                    </div>
                                    <input type="text" name="table_search" class="form-control float-right"
                                        placeholder="Buscar">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>Categoria</th>
                                            <th>Sucursal</th>
                                            <th>Estado</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>P001</td>
                                            <td>producto1</td>
                                            <td>Este es el producto 1</td>
                                            <td>10</td>
                                            <td>1500</td>
                                            <td>categoria 1</td>
                                            <td>Sucursal 1</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarproducto') }}">editar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>P002</td>
                                            <td>producto2</td>
                                            <td>Este es el producto 2</td>
                                            <td>20</td>
                                            <td>2500</td>
                                            <td>categoria 2</td>
                                            <td>Sucursal 2</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarproducto') }}">editar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>P003</td>
                                            <td>producto3</td>
                                            <td>Este es el producto 3</td>
                                            <td>30</td>
                                            <td>1000</td>
                                            <td>categoria 3</td>
                                            <td>Sucursal 1</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarproducto') }}">editar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>P004</td>
                                            <td>producto4</td>
                                            <td>Este es el producto 4</td>
                                            <td>50</td>
                                            <td>11500</td>
                                            <td>categoria 4</td>
                                            <td>Sucursal 3</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarproducto') }}">editar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>P005</td>
                                            <td>producto5</td>
                                            <td>Este es el producto 5</td>
                                            <td>80</td>
                                            <td>15300</td>
                                            <td>categoria 3</td>
                                            <td>Sucursal 2</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarproducto') }}">editar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>P006</td>
                                            <td>producto6</td>
                                            <td>Este es el producto 6</td>
                                            <td>100</td>
                                            <td>15800</td>
                                            <td>categoria 1</td>
                                            <td>Sucursal 3</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarproducto') }}">editar</a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection
