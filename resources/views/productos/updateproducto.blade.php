@extends('base')
@section('seccion')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-12">
                    <div class="col-sm-12">
                        <h1>FORMULARIO PARA EDITAR PRODUCTO</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Detalles del producto</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-10">

                                <!-- formulario -->
                                <form class="form-horizontal" action="{{ route('buscarproducto') }}">
                                    <div class="form-group row">
                                        <label for="inputCodigo" class="col-sm-2 col-form-label">Codigo</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputCodigo" value="P001">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputNombre" class="col-sm-2 col-form-label">Nombre</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputNombre" value="producto1">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputDescripcion" class="col-sm-2 col-form-label">Descripcion</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="inputDescripcion" rows="3">Este es el producto 1</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputCantidad" class="col-sm-2 col-form-label">Cantidad</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" id="inputCantidad" value="10">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputCategoria" class="col-sm-2 col-form-label">Categoria</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" id="inputCategoria">
                                                <option>Seleccionar categoria</option>
                                                <option selected>categoria 1</option>
                                                <option>categoria 2</option>
                                                <option>categoria 3</option>
                                                <option>categoria 4</option>
                                                <option>categoria 5</option>
                                              </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputSucursal" class="col-sm-2 col-form-label">Sucursal</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" id="inputSucursal">
                                                <option>Seleccionar Sucursal</option>
                                                <option selected>Sucursal 1</option>
                                                <option>Sucursal 2</option>
                                                <option>Sucursal 3</option>
                                              </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEstado" class="col-sm-2 col-form-label">Estado</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" id="inputEstado">
                                                <option>Seleccionar Estado</option>
                                                <option selected>Activo</option>
                                                <option>Inactivo</option>
                                              </select>
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-center h-100">
                                        <div class="col-sm-10 align-self-center text-center">
                                            <button type="submit" class="btn btn-success">Actualizar</button>
                                            <button type="submit" class="btn btn-warning">Eliminar</button>
                                            <a href="{{ route('buscarproducto') }}" class="btn btn-danger">Cancelar</a>
                                        </div>
                                    </div>
                                </form>
                                <!-- end formulario -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection
