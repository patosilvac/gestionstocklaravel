@extends('base')
@section('seccion')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>LISTADO GENERAL DE USUARIOS</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">

                <!-- row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="input-group-append">
                                    <div class="col-sm-4">
                                        <select class="form-control" id="inputSucursal">
                                            <option>Seleccionar opcion de busqueda</option>
                                            <option>Usuario</option>
                                            <option>Nombre</option>
                                            <option>Apellido</option>
                                        </select>
                                    </div>
                                    <input type="text" name="table_search" class="form-control float-right"
                                        placeholder="Buscar">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>Usuario</th>
                                            <th>E-mail</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Sucursal</th>
                                            <th>Estado</th>
                                            <th>Editar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>10001</td>
                                            <td>psilva@ciisa.cl</td>
                                            <td>Patricio</td>
                                            <td>Silva</td>
                                            <td>Sucursal 1</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarusuario') }}">editar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>10002</td>
                                            <td>aalvarez@ciisa.cl</td>
                                            <td>Ada</td>
                                            <td>Alvarez</td>
                                            <td>Sucursal 2</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarusuario') }}">editar</a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>10003</td>
                                            <td>ssilva@ciisa.cl</td>
                                            <td>Soledad</td>
                                            <td>Silva</td>
                                            <td>Sucursal 3</td>
                                            <td>Activo</td>
                                            <td>
                                                <a class="btn btn-success" href="{{ route('editarusuario') }}">editar</a>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection


