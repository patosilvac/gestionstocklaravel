<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>GESTION STOCK</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
        integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
        crossorigin="anonymous" />
    <!-- IonIcons -->
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('./css/adminlte.min.css') }}">
</head>

<!-- AREA DE BODY -->

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-primary navbar-dark sticky-top">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <!--
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('inicio') }}" class="nav-link">Inicio</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('productos') }}" class="nav-link">Productos</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('sucursales') }}" class="nav-link">Sucursales</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('categorias') }}" class="nav-link">Categorias</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="{{ route('usuarios') }}" class="nav-link">Usuarios</a>
                </li>
            -->
            </ul>
            <ul class="navbar-nav ml-auto float-right">
                <li class="nav-item text-nowrap px-md-5">
                    <a class="btn btn-light" href="{{route('login')}}">Cerrar Sesion</a>
                </li>
            </ul>

            <!-- SEARCH FORM
            <form class="ml-3 float-right">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search"
                        aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
            -->

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{ route('inicio') }}" class="brand-link">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="card text-white bg-primary text-center" style="max-width: 15rem;">
                            <div class="card-body">
                                <h2 class="card-title">GS</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{ URL::asset('./img/user1-128x128.jpg') }}" class="img-circle elevation-2"
                            alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block">Patricio Silva</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">

                        <!-- Area de menu Productos -->
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{ route('buscarproducto') }}" class="nav-link">
                                <i class="nav-icon fab fa-product-hunt"></i>
                                <p>
                                    PRODUCTOS
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('agregarproducto') }}" class="nav-link">
                                        <i class="nav-icon fas fa-plus text-success"></i>
                                        <p>Agregar Producto</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('buscarproducto') }}" class="nav-link">
                                        <i class="nav-icon fas fa-search text-primary"></i>
                                        <p>Consultar producto</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        {{-- <!-- Area de menu Sucursales -->
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{ route('buscarsucursal') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            SUCURSALES
                            <i class="fas fa-angle-left right"></i>
                        </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('agregarsucursal') }}" class="nav-link">
                                    <i class="nav-icon fas fa-plus text-success"></i>
                                    <p>Agregar Sucursal</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('buscarsucursal') }}" class="nav-link">
                                    <i class="nav-icon fas fa-search text-info"></i>
                                    <p>Consultar Sucursal</p>
                                </a>
                            </li>
                        </ul>
                        </li>

                        <!-- Area de menu Categorias -->
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{ route('buscarcategoria') }}" class="nav-link">
                                <i class="nav-icon fas fa-th"></i>
                                <p>
                                    CATEGORIAS
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('agregarcategoria') }}" class="nav-link">
                                        <i class="nav-icon fas fa-plus text-success"></i>
                                        <p>Agregar Categoria</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('buscarcategoria') }}" class="nav-link">
                                        <i class="nav-icon fas fa-search text-info"></i>
                                        <p>Consultar Categoria</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Area de menu Usuarios -->
                        <li class="nav-item has-treeview menu-open">
                            <a href="{{ route('buscarusuario') }}" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    USUARIOS
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route('agregarusuario') }}" class="nav-link">
                                        <i class="nav-icon fas fa-user-plus text-success"></i>
                                        <p>Agregar Usuario</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{ route('buscarusuario') }}" class="nav-link">
                                        <i class="nav-icon fas fa-search text-info"></i>
                                        <p>Consultar Usuarios</p>
                                    </a>
                                </li>
                            </ul>
                        </li> --}}

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Start section -->
        @yield('seccion')
        <!-- end section -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <strong>Copyright &copy; RA_PS_CQ - CIISA</strong>
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 1.0
            </div>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('../plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('../plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE -->
    <script src="{{ asset('../js/adminlte.js') }}"></script>

</body>

</html>
