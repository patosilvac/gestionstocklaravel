@extends('base')
@section('seccion')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-12">
                    <div class="col-sm-12">
                        <h1>FORMULARIO PARA AGREGAR CATEGORIA</h1>
                    </div>
                </div>
            </div>
            <!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Nueva Categoria</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-10">

                                <!-- formulario -->
                                <form class="form-horizontal">
                                    <div class="form-group row">
                                        <label for="inputCodigo" class="col-sm-2 col-form-label">Codigo</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputCodigo">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputNombre" class="col-sm-2 col-form-label">Nombre</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="inputNombre">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEstado" class="col-sm-2 col-form-label">Estado</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" id="inputEstado">
                                                <option>Seleccionar Estado</option>
                                                <option>Activo</option>
                                                <option>Inactivo</option>
                                              </select>
                                        </div>
                                    </div>                                
                                    <div class="form-group row justify-content-center h-100">
                                        <div class="col-sm-10 align-self-center text-center">
                                            <button type="submit" class="btn btn-success">Agregar</button>
                                            <a href="{{ route('buscarcategoria') }}" class="btn btn-danger">Cancelar</a>
                                        </div>
                                    </div>
                                </form>
                                <!-- end formulario -->
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection
