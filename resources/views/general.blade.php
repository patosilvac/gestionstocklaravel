@extends('base')
@section('seccion')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>TABLERO DE RESUMEN</h1>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Ultimos productos ingresados</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                        <tr>
                                        <tr>
                                            <th>Codigo</th>
                                            <th>Nombre</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>Categoria</th>
                                            <th>Sucursal</th>
                                        </tr>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>P001</td>
                                            <td>producto1</td>
                                            <td>10</td>
                                            <td>1500</td>
                                            <td>categoria 1</td>
                                            <td>Sucursal 1</td>
                                        </tr>
                                        <tr>
                                            <td>P002</td>
                                            <td>producto2</td>
                                            <td>20</td>
                                            <td>2500</td>
                                            <td>categoria 2</td>
                                            <td>Sucursal 2</td>
                                        </tr>
                                        <tr>
                                            <td>P003</td>
                                            <td>producto3</td>
                                            <td>30</td>
                                            <td>1000</td>
                                            <td>categoria 3</td>
                                            <td>Sucursal 1</td>
                                        </tr>
                                        <tr>
                                            <td>P004</td>
                                            <td>producto4</td>
                                            <td>50</td>
                                            <td>11500</td>
                                            <td>categoria 4</td>
                                            <td>Sucursal 3</td>
                                        </tr>
                                        <tr>
                                            <td>P005</td>
                                            <td>producto5</td>
                                            <td>80</td>
                                            <td>15300</td>
                                            <td>categoria 3</td>
                                            <td>Sucursal 2</td>
                                        </tr>
                                        <tr>
                                            <td>P006</td>
                                            <td>producto6</td>
                                            <td>100</td>
                                            <td>15800</td>
                                            <td>categoria 1</td>
                                            <td>Sucursal 3</td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Usuarios activos</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap">
                                    <thead>
                                        <tr>
                                            <th>Usuario</th>
                                            <th>E-mail</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Sucursal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>10001</td>
                                            <td>psilva@ciisa.cl</td>
                                            <td>Patricio</td>
                                            <td>Silva</td> 
                                            <td>Sucursal 1</td>
                                        </tr>
                                        <tr>
                                            <td>10002</td>
                                            <td>aalvarez@ciisa.cl</td>
                                            <td>Ada</td>
                                            <td>Alvarez</td>
                                            <td>Sucursal 2</td>
                                        </tr>
                                        <tr>
                                            <td>10003</td>
                                            <td>ssilva@ciisa.cl</td>
                                            <td>Soledad</td>
                                            <td>Silva</td>
                                            <td>Sucursal 3</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->


                    </div>
                    <!-- /.col -->
                </div>

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

@endsection
