<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>Login GS</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ URL::asset('./css/all.min.css') }}">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ URL::asset('./css/signin.css') }}">
</head>

<body class="text-center">

    <form class="form-signin" method="GET" action="{{ route('inicio') }}">
        <!-- logo -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="card text-white bg-primary mb-4 text-center" style="max-width: 15rem;">
                    <div class="card-body">
                        <h2 class="card-title">GS</h2>
                    </div>
                </div>
            </div>
        </div>

        <h1 class="h4 mb-3 font-weight-normal">SISTEMA GESTION STOCK</h1>
        <label for="inputUser" class="sr-only">Email</label>
        <input type="text" id="inputUser" class="form-control" placeholder="Usuario" required autofocus>
        <label for="inputPassword" class="sr-only">contraseña</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Contraseña" required>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" value="remember-me"> Recuerdame
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
        <p class="mt-5 mb-3 text-muted">&copy; RA_PS_CQ - CIISA</p>
    </form>
</body>

</html>
