<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

//carga login
Route::get('/', function () {
    return view('login');
});

Route::get('login', function () {
    return view('login');
})->name('login');

//main o dashboard
Route::get('inicio', function () {
    return view('general');
})->name('inicio');

//ruta para productos
Route::get('productos', function () {
    return view('productos/findproducto');
})->name('productos');

Route::get('productos/buscarproducto', function () {
    return view('productos/findproducto');
})->name('buscarproducto');

Route::get('productos/agregarproducto', function () {
    return view('productos/addproducto');
})->name('agregarproducto');

Route::get('productos/editarproducto', function () {
    return view('productos/updateproducto');
})->name('editarproducto');

//ruta para sucursales
Route::get('sucursales', function () {
    return view('sucursales/findsucursal');
})->name('sucursales');

Route::get('sucursales/buscarsucursal', function () {
    return view('sucursales/findsucursal');
})->name('buscarsucursal');

Route::get('sucursales/agregarsucursal', function () {
    return view('sucursales/addsucursal');
})->name('agregarsucursal');

Route::get('sucursales/editarsucursal', function () {
    return view('sucursales/updatesucursal');
})->name('editarsucursal');

//ruta para categorias
Route::get('categorias', function () {
    return view('categorias/findcategoria');
})->name('categorias');

Route::get('categorias/buscarusuario', function () {
    return view('categorias/findcategoria');
})->name('buscarcategoria');

Route::get('categorias/agregarusuario', function () {
    return view('categorias/addcategoria');
})->name('agregarcategoria');

Route::get('categorias/editarusuario', function () {
    return view('categorias/updatecategoria');
})->name('editarcategoria');

//ruta para usuarios
Route::get('usuarios', function () {
    return view('usuarios/findusuario');
})->name('usuarios');

Route::get('usuarios/buscarusuario', function () {
    return view('usuarios/findusuario');
})->name('buscarusuario');

Route::get('usuarios/agregarusuario', function () {
    return view('usuarios/addusuario');
})->name('agregarusuario');

Route::get('usuarios/editarusuario', function () {
    return view('usuarios/updateusuario');
})->name('editarusuario');

