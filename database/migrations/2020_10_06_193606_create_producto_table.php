<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto', function (Blueprint $table) {
            $table->increments('id_producto');
            $table->string('nombre_pro');
            $table->string('desc_prod');
            $table->unsignedInteger('categoria_id');
            $table->foreign('categoria_id')->references('id_categoria')->on('categoria')->onDelete('cascade');
            $table->unsignedInteger('estado_id');
            $table->foreign('estado_id')->references('id_estado')->on('estado')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto');
    }
}
