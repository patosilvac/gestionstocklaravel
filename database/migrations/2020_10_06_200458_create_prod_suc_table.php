<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdSucTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prod_suc', function (Blueprint $table) {
            $table->string('cantidad');
            $table->integer('precio');
            $table->unsignedInteger('producto_id');
            $table->foreign('producto_id')->references('id_producto')->on('producto');
            $table->unsignedInteger('sucursal_id');
            $table->foreign('sucursal_id')->references('id_sucursal')->on('sucursale');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prod_suc');
    }
}
